#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
"""
Usage: doomsday [OPTIONS] [YEAR]

Positional arguments:
  YEAR                  year to find doomsday for

Optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show version information and exit
  -c, --copyright       show copying policy and exit
  -r, --reminder        show doomsday algorithm reminder and exit
  -f FORMAT, --format FORMAT
                        date format (default ‘%A’: full weekday name)
"""
##############################################################################
# This program is free software; you can redistribute it and/or modify it    #
# under the terms of the GNU General Public License as published by the Free #
# Software Foundation; either version 3 of the License, or (at your option)  #
# any later version.                                                         #
#                                                                            #
# This program is distributed in the hope that it will be useful, but with-  #
# out any warranty; without even the implied warranty of merchantability or  #
# fitness for a particular purpose.  See the GNU General Public License for  #
# more details.  <http://gplv3.fsf.org/>                                     #
##############################################################################

import argparse
import datetime
import locale
import os
import sys

from contextlib import suppress
from datetime import (MINYEAR, MAXYEAR)
from typing import Union

with suppress(locale.Error):
    _ = locale.setlocale(locale.LC_ALL, '')

# If run with Python 2, the script has probably already barfed over the
# “from typing import Union”, but let's check the Python version anyway:
if sys.version_info < (3, 5):
    print('Python 3.5+ required', file=sys.stderr)
    sys.exit(1)

__author__ = 'Klaus Alexander Seistrup <klaus@seistrup.dk>'
__revision__ = '2025-01-03'
__version__ = f'0.0.8 ({__revision__})'
__copyright__ = f"""
doomsday {__version__}

Copyright © 2015 Klaus Alexander Seistrup <klaus@seistrup.dk>

This is free software; see the source for copying conditions.  There is no
warranty; not even for merchantability or fitness for a particular purpose.

Fork me on Codeberg: https://codeberg.org/kas/doomsday
"""

EPILOG = f"""
YEAR should be in range {MINYEAR}‥{MAXYEAR}.

See date(1) for common sequences for FORMAT.
"""

REMINDER = """
Doomsday is the last day of February — i.e., February 28 in normal years
and February 29 in leap years.

Even months (April, June, August, October and December): The Nth of month
N is also a doomsday:
  · April 4
  · June 6
  · August 8
  · October 10
  · December 12

Odd months (May, July, September, November): Use the mnemonic
“I work from 9-5 at the 7-Eleven”:
  · for the  9th month, doomsday is the  5th
  · for the  5th month, doomsday is the  9th
  · for the  7th month, doomsday is the 11th
  · for the 11th month, doomsday is the  7th

January: Doomsday is January 3rd three years out of four, the non-leap
years, and January 4th only in the fourth year, the years divisible by 4.

For other years and centuries, please consult https://rudy.ca/doomsday.html
"""

ErrorLike = Union[Exception, str]


def die(reason: ErrorLike = '') -> None:
    """Optionally write a message and exit with an exit code"""
    if reason:
        print(reason, file=sys.stderr)

    sys.exit(1 if reason else 0)


def doomsday(year: int) -> datetime.date:
    """Returns last day of february in <year>"""
    if year < MINYEAR or year > MAXYEAR:
        die(f'YEAR should be in range {MINYEAR}‥{MAXYEAR}')

    try:
        # Doomsday is the last day of February. Since we don't know
        # if we have a leap year or not, and since we can't be arsed
        # to calculate the leapness, we just calculate the day before
        # 1st March:
        dday = datetime.date(year, 3, 1) - datetime.timedelta(days=1)
    except ValueError as error:
        die(f'{error}: {year}')

    return dday


def main(progname: str) -> int:
    """Main entry point"""
    parser = argparse.ArgumentParser(
        prog=progname,
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=EPILOG
    )
    parser.add_argument(
        '--version', action='version', version=f'%(prog)s/{__version__}',
        help='show version information and exit'
    )
    parser.add_argument(
        '--copyright', action='version', version=__copyright__,
        help='show copying policy and exit'
    )
    parser.add_argument(
        '-m', '-r', '--reminder', action='version', version=REMINDER,
        help='show doomsday algorithm reminder and exit'
    )
    parser.add_argument(
        '-f', '--format', default='%A',
        help='date format (default ‘%%A’: full weekday name)'
    )
    parser.add_argument(
        'YEAR', nargs='?', type=int, help='year to find doomsday for'
    )
    args = parser.parse_args()

    year = args.YEAR or datetime.date.today().year

    try:
        dato = doomsday(year)
        sfmt = f'{{:{args.format}}}'
        result = sfmt.format(dato)
    except ValueError as error:
        die(error)

    print(result)

    return 0


if __name__ == '__main__':
    sys.exit(main(os.path.basename(sys.argv[0])))

# eof

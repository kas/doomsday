# Doomsday

The [Doomsday Algorithm](https://rudy.ca/doomsday.html) gives the day of
the week for any date (and you can do it in your head).

## Usage

```sh
$ doomsday --help
Usage: doomsday [OPTIONS] [YEAR]

positional arguments:
  YEAR                  year to find doomsday for

optional arguments:
  -h, --help            show this help message and exit
  --version             show version information and exit
  --copyright           show copying policy and exit
  -m, -r, --reminder    show doomsday algorithm reminder and exit
  -f FORMAT, --format FORMAT
                        date format (default ‘%A’: full weekday name)
```

## Examples

```sh
$ doomsday              # current year (2022)
Monday
$ doomsday -f '%F'      # use a custom format
2022-02-28
$ doomsday 1968         # 1968, a leap year
Thursday
```

## Requirements

Python 3.5+

## Installation

Just drop it somewhere in your `$PATH`.

Don't worry, be happy :smile:
